import "./styles/style.css";

import Header from "./components/header/header";
import Landing from "./components/landing/landing";
import Main from "./components/main/main";
import Feature from "./components/feature/feature";
import Form from "./components/form/form";
import Footer from "./components/footer/footer";

import featureBackgroundImage from "./assets/images/music-titles.png";
import formBackgroundImage from "./assets/images/background-page-sign-up.png";

const header = Header();
const main = Main();
const landing = Landing();
const feature = Feature();
const form = Form();
const footer = Footer();

/* global document */
main.append(landing);

const app = document.querySelector("#app");
app.append(header);
app.append(main);
app.append(footer);

const displayAllHeaderElements = () => {
  const links = header.querySelectorAll("a");
  links.forEach(link => {
    if (link.classList.contains("hidden")) {
      link.classList.remove("hidden");
    }
  });
};

const featureHeaderLink = header.querySelector("#discover");
const renderFeaturePage = () => {
  main.innerHTML = "";
  main.append(feature);
  displayAllHeaderElements();
  app.style.background = `url(${featureBackgroundImage}) no-repeat center right content-box`;
  featureHeaderLink.classList.add("hidden");
  featureHeaderLink.removeEventListener("click", renderFeaturePage);
};
featureHeaderLink.addEventListener("click", renderFeaturePage);

const joinHeaderLink = header.querySelector("#join");
const joinNowButton = landing.querySelector("#joinNowButton");
const renderSignUpPage = () => {
  main.innerHTML = "";
  main.append(form);
  displayAllHeaderElements();
  app.style.background = `url(${formBackgroundImage}) no-repeat bottom right`;
  joinHeaderLink.classList.add("hidden");
  joinHeaderLink.removeEventListener("click", renderSignUpPage);
  joinNowButton.removeEventListener("click", renderSignUpPage);
};
joinHeaderLink.addEventListener("click", renderSignUpPage);
joinNowButton.addEventListener("click", renderSignUpPage);
