import "./main.css";

export default function createMain() {
  /* global document */
  const main = document.createElement("main");
  main.classList.add("page-main");

  return main;
}
