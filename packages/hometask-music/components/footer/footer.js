import "./footer.css";

export default function createFooter() {
  /* global document */
  const footer = document.createElement("footer");
  footer.classList.add("page-footer");

  footer.innerHTML = `
    <nav class="page-footer__nav">
        <ul class="page-footer__list">
            <li class="page-footer__item">
                <a href="#">About As</a>
            </li>
            <li class="page-footer__item">
                <a href="#">Contact</a>
            </li>
            <li class="page-footer__item">
                <a href="#">CR Info</a>
            </li>
            <li class="page-footer__item">
                <a href="#">Twitter</a>
            </li>
            <li class="page-footer__item">
                <a href="#">Facebook</a>
            </li>
        </ul>
    </nav>
    `;

  return footer;
}
