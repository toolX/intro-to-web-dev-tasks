import "./form.css";

export default function createForm() {
  /* global document */
  const form = document.createElement("section");
  form.classList.add("page-form");

  form.innerHTML = `
        <form class="form" method="POST" action="/">
            <div class="form__wrapper">
                <div class="form__row">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" required>
                </div>
                <div class="form__row">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" required>
                </div>
                <div class="form__row">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" required>
                </div>
            </div>
            <input class="btn  form__btn" type="submit" value="Join now">
        </form>
      `;

  return form;
}
