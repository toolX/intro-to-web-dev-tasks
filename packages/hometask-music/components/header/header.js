import "./header.css";

export default function createHeader() {
  /* global document */
  const header = document.createElement("header");
  header.classList.add("page-header");

  header.innerHTML = `
        <a href="#" class="page-logo">
            Simo
        </a>
        <nav class="page-nav">
            <ul class="page-nav__list">
                <li class="page-nav__item">
                    <a href="#" id="discover">Discover</a>
                </li>
                <li class="page-nav__item">
                    <a href="#" id="join">Join</a>
                </li>
                <li class="page-nav__item">
                    <a href="#">Sign In</a>
                </li>
            </ul>
        </nav>
    `;

  return header;
}
