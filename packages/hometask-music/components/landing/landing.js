import "./landing.css";

export default function createLanding() {
  /* global document */
  const landing = document.createElement("section");
  landing.classList.add("landing");

  landing.innerHTML = `
    <h1 class="landing__header">Feel the music</h1>
    <p class="landing__text">Stream over 10 million songs with one click</p>
    <a class="btn  landing__btn" href="#" id="joinNowButton">Join now</a>
  `;

  return landing;
}
