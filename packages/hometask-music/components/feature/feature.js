import "./feature.css";

export default function createFeature() {
  /* global document */
  const feature = document.createElement("section");
  feature.classList.add("feature");

  feature.innerHTML = `
      <h1 class="feature__header">Discover new music</h1>
      <ul class="feature__list">
        <li class="feature__item">
            <a class="btn  feature__btn" href="#">Charts</a>
        </li>
        <li class="feature__item">
            <a class="btn  feature__btn" href="#">Songs</a>
        </li>
        <li class="feature__item">
            <a class="btn  feature__btn" href="#">Artists</a>
        </li>
      </ul>
      <p class="feature__text">By joing you can benefit by listening to the latest albums released</p>
    `;

  return feature;
}
