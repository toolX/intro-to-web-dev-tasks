import logo from "../../../assets/icons/logo.svg";
import searchIcon from "../../../assets/icons/search.svg";
import shoppingCart from "../../../assets/icons/box.svg";
import userIcon from "../../../assets/icons/user.svg";
import burgerMenu from "../../../assets/icons/menu.svg";

const Header = () => `
  <header class="page-header">
    <a class="page-logo" href="#">
      <img src=${logo} alt="Link to main page">
    </a>
    <nav class="page-nav">
      <ul class="page-nav__list">
        <li class="page-nav__item">
          <a class="page-nav__link" href="#">
            <img src=${searchIcon} alt="Search Link">
          </a>
        </li>
        <li class="page-nav__item">
          <a class="page-nav__link" href="#">
            <img src=${shoppingCart} alt="Shopping Link">
          </a>
        </li>
        <li class="page-nav__item">
          <a class="page-nav__link" href="#">
            <img src=${userIcon} alt="User Link">
          </a>
        </li>
      </ul>
    </nav>
    <a class="page-burger" href="#">
      <img src=${burgerMenu} alt="Page Menu">
    </a>
  </header>
`;

export default Header;
