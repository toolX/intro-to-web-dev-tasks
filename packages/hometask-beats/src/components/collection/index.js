import leftArrow from "../../../assets/icons/arrow-left.svg";
import productOne from "../../../assets/images/colour-02.png";
import productTwo from "../../../assets/images/colour-01.png";
import productThree from "../../../assets/images/colour-03.png";
import rightArrow from "../../../assets/icons/arrow-right.svg";
import feature from "../../../assets/images/feature.png";
import battery from "../../../assets/icons/Group1.png";
import bluetooth from "../../../assets/icons/Group2.png";
import microphone from "../../../assets/icons/Group3.png";

const Collection = () => `
  <section class="colour">
    <h2 class="colour__header">Our Latest <br> colour collection 2021</h2>
    <section class="slider  colour__slider">
      <button class="btn-arrow" type="button">
        <img src=${leftArrow} alt="Arrow left">
      </button>
      <img class="slider__img" src=${productOne} alt="Red_Hearphones">
      <img class="slider__main-img" src=${productTwo} alt="Blue_Hearphone">
      <img class="slider__img" src=${productThree} alt="Orange_Hearphones">
      <button class="btn-arrow  btn-arrow--reverse" type="button">
        <img src=${rightArrow} alt="Arrow right">
      </button>
    </section>
    <section class="features  colour__features">
      <div class="features__wrapper">
        <h2 class="features__header">Good headphones and loud music is all you need</h2>
        <div class="features__item">
          <img class="features__img" src=${battery} alt="Battery">
          <div class="feartures__info">
            <h2 class="features__subheader">Battery</h2>
            <p class="features__description">Battery 6.2V-AAC codec</p>
            <a href="#" class="features__link">Learn more</a>
          </div>
        </div>
        <div class="features__item">
          <img class="features__img" src=${bluetooth} alt="Bluetooth">
          <div class="feartures__info">
            <h2 class="features__subheader">Bluetooth</h2>
            <p class="features__description">Battery 6.2V-AAC codec</p>
            <a href="#" class="features__link">Learn more</a>
          </div>
        </div>
        <div class="features__item">
          <img class="features__img" src=${microphone} alt="microphone">
          <div class="feartures__info">
            <h2 class="features__subheader">Microphone</h2>
            <p class="features__description">Battery 6.2V-AAC codec</p>
            <a href="#" class="features__link">Learn more</a>
          </div>
        </div>
      </div>
      <img class="features__preview" src=${feature} alt="Yellow_Hearphones">
    </section>
  </section>
`;

export default Collection;
