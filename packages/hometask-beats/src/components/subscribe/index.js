const Subscribe = () => `
  <section class="subscribe">
    <h2 class="subscribe__header">Subscribe</h2>
    <p class="subscribe__description">Lorem ipsum dolor sit amet, consectetur</p>
    <form class="subscribe__form" action="/" method="POST">
      <input class="subscribe__input" type="email" id="email" placeholder="Enter Your email address">
      <button class="subscribe__btn" type="submit">Subscribe</button>
    </form>
  </section>
`;

export default Subscribe;
