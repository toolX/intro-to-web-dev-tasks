import heroImg from "../../../assets/images/hero.png";

const Hero = () => `
  <main class="hero">
    <img src=${heroImg} alt="Pro Bits Headphones" class="hero__img">
    <div class="hero__info">
      <h2 class="hero__subtitle">
        Hear it. Feel it
      </h2>
      <h1 class="hero__title">Move with the music</h1>
      <div class="hero__price">
        <span class="hero__newprice">$ 435</span>
        <span class="hero__oldprice">$ 465</span>
      </div>
      <a class="hero__btn" href="#">
        BUY NOW
      </a>
    </div>
  </main>
`;

export default Hero;
