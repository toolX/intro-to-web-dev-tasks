import footerLogo from "../../../assets/icons/logo.svg";
import instagram from "../../../assets/icons/instagram.svg";
import twitter from "../../../assets/icons/twitter.svg";
import facebook from "../../../assets/icons/facebook.svg";

const Footer = () => `
  <footer class="page-footer">
    <a class="page-logo" href="#">
      <img src=${footerLogo} alt="Link to main page">
    </a>
    <ul class="footer__nav">
      <li class="footer__nav-item">
        <a href="#">Home</a>
      </li>
      <li class="footer__nav-item">
        <a href="#">About</a>
      </li>
      <li class="footer__nav-item">
        <a href="#">Product</a>
      </li>
    </ul>
    <ul class="social  footer__social">
      <li class="social__item">
        <a href="#">
          <img class="social__icon" src=${instagram} alt="Instagram" class="insta_footer_logo">
        </a>
      </li>
      <li class="social__item">
        <a href="#">
          <img class="social__icon" src=${twitter} alt="Twitter" class="twitter_footer_logo">
        </a>
      </li>
      <li class="social__item">
        <a href="#">
          <img class="social__icon" src=${facebook} alt="Facebook" class="fb_footer_logo">
        </a>
      </li>
    </ul>
  </footer>
`;

export default Footer;
