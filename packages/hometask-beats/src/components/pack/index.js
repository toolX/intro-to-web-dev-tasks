import boxingImg from "../../../assets/images/boxing.png";

const Pack = () => `
  <section class="pack">
    <img src=${boxingImg} alt="Pack image" class="pack__img">
    <div class="pack__wrapper">
      <h2 class="pack__header">Whatever you get <br> in the box</h2>
      <ul class="pack__list">
        <li class="pack__item">
          5A Charger
        </li>
        <li class="pack__item">
          Extra battery
        </li>
        <li class="pack__item">
          Sophisticated bag
        </li>
        <li class="pack__item">
          User manual guide
        </li>
      </ul>
    </div>
  </section>`;

export default Pack;
