import productOne from "../../../assets/images/product-03.png";
import productOneBg from "../../../assets/images/Group4.png";
import productTwo from "../../../assets/images/product-02.png";
import productTwoBg from "../../../assets/images/Group6.png";
import productThree from "../../../assets/images/product-01.png";
import productThreeBg from "../../../assets/images/Group5.png";
import starImg from "../../../assets/icons/star.jpeg";

const products = `
  <section class="products">
    <h2 class="product__header">Our Latest Product</h2>
    <p class="product__description">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis nunc
      ipsum aliquam, ante.
    </p>
    <div class="products__wrapper">
      <div class="product__wrapper  product__wrapper--first">
        <div>
          <img src=${productOne} alt="Purple Headphones" class="product__img  product__img--first">
          <img src=${productOneBg} class="product__bg">
        </div>
        <div class="product__rating">
          <ul class="product__stars">
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="One star">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Two stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Three stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Four stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Five stars">
              </a>
            </li>
          </ul>
          <span class="product__rating-value">4.50</span>
        </div>
        <div class="product__price">
          <span>Red Headphone</span>
          <span>$ 256</span>
        </div>
      </div>

      <div class="product__wrapper  product__wrapper--second">
        <div>
          <img src=${productTwo} alt="Blue Headphones" class="product__img  product__img--second">
          <img src=${productTwoBg} class="product__bg">
        </div>
        <div class="product__rating">
          <ul class="product__stars">
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="One star">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Two stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Three stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Four stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Five stars">
              </a>
            </li>
          </ul>
          <span class="product__rating-value">4.50</span>
        </div>
        <div class="product__price">
          <span>Blue Headphone</span>
          <span>$ 235</span>
        </div>
      </div>

      <div class="product__wrapper  product__wrapper--third">
        <div>
          <img src=${productThree} alt="Green Headphones" class="product__img  product__img--third">
          <img src=${productThreeBg} class="product__bg">
        </div>
        <div class="product__rating">
          <ul class="product__stars">
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="One star">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Two stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Three stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Four stars">
              </a>
            </li>
            <li>
              <a href="#">
                <img class="product__star" src=${starImg} alt="Five stars">
              </a>
            </li>
          </ul>
          <span class="product__rating-value">4.50</span>
        </div>
        <div class="product__price">
          <span>Blue Headphone</span>
          <span>$ 245</span>
        </div>
      </div>
    </div>
  </section>
`;

const Product = () => products;
export default Product;
