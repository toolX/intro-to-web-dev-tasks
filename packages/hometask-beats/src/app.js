import Header from "./components/header/index";
import Hero from "./components/hero/index";
import Collection from "./components/collection/index";
import Product from "./components/product/index";
import Pack from "./components/pack/index";
import Subscribe from "./components/subscribe/index";
import Footer from "./components/footer/index";
import Container from "./components/container/index";

/* global document */
const app = document.querySelector(".app");

const render = () => {
  app.innerHTML = `
  ${Container(Header(), Hero())}
  ${Collection()}
  ${Product()}
  ${Pack()}
  ${Subscribe()}
  ${Footer()}
  `;
};

export default render;
